import dao.Appointment_dao;
import dao.Users_dao;
import dataAccess.Appointment_repo;
import frontAPI.API_Appointment;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {

        Appointment_repo rep = new Appointment_repo();
        List<Appointment_dao> list = rep.getAllAppointments();

        for(Appointment_dao x : list){
            System.out.println(x);
            System.out.println(x.getDuration()+ " Minutes");
        }


//        Appointment_dao insert = new Appointment_dao("Test","This is a test","10:00-11:00", LocalDate.of(2022, Month.DECEMBER,1),new Users_dao("Ranni"));
//        Appointment_repo repo = new Appointment_repo();
//        System.out.println(repo.getAllAppointments());
//        System.out.println(repo.createAppointment(insert));
//        System.out.println(repo.getAllAppointments());

//        LocalTime localTime1 = LocalTime.of(10,30);
//        LocalTime localTime2 = LocalTime.of(13,30);
//
//        LocalTime duration = LocalTime.of(localTime2.getHour()-localTime1.getHour(),localTime2.getMinute()-localTime1.getMinute());
//
//        System.out.println("Time 1 is : "+localTime1);
//        System.out.println("Time 2 is : "+localTime2);
//        System.out.println("Duration is : "+duration.getHour()+" hours and "+duration.getMinute()+" Minutes");

    }
}
