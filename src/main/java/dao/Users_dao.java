package dao;

import java.sql.ResultSet;

public class Users_dao {
    private Integer uid;
    private String name;

    public Users_dao(String na){
        setName(na);
    }
    public Users_dao(ResultSet res){
        try {
            setName(res.getString("name"));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public String getName() {
        return name;
    }

    public Integer getUid() {return uid;}

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Users_dao{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                '}';
    }
}
