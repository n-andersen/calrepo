package dataLogic;

import dao.Appointment_dao;
import dao.Users_dao;
import dataAccess.Appointment_repo;
import dataAccess.User_repo;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public class CalendarUtil {// todo rename class
    final static User_repo user_repo = new User_repo();
    final static Appointment_repo appRepo = new Appointment_repo();

    public static boolean validateDate(String toValidate){
        if(LocalDate.parse(toValidate).equals(LocalDate.class))
            return true;
        return false;
    }
    public static boolean validateInt(String toValidate){
        try {
            Integer.parseInt(toValidate);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    public static Integer getCount(List toCount){
        return toCount.size();
    }

    public static boolean validateAppointment(Appointment_dao toValidate){
        if(toValidate.getTitle()==null || toValidate.getTitle() == "")
            return false;
        if(toValidate.getDate() == null)
            return false;
        if(!validateUser(toValidate.getOwner()))
            return false;
        if(toValidate.getText().length() > 250)
            return false;

        return true;
    }
    public static boolean validateUser(Users_dao toValidate){
        if(user_repo.userExists(toValidate.getName()))
            return true;
        return false;
    }
    public static boolean validateTime(String toValidate){
        return true;
    }
}
