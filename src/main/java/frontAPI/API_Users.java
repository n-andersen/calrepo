package frontAPI;

import dataAccess.User_repo;

import java.text.DateFormat;

public class API_Users {
    User_repo repo = new User_repo();
    public String getUsers(){
        String result;
        try{
            result = repo.getAllUsers().toString();
            return result;
        }catch (Exception e){
            return e.getMessage();
        }
    }
    public String getUserByName(String name){
        String result;
        try {
            result = repo.getUserByName(name).toString();
            return result;
        }catch (Exception e){
            return e.getMessage();
        }
    }
    public Integer getCountAppointments(Long lon){
        return 0;

    }
}
