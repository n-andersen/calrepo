import java.time.LocalDateTime;
import java.util.List;

public class Appointment {
    //Fields
    private String name,message,owner,title;
    private List<String> participants;
    private LocalDateTime createdDate,scheduledDate;
    //Getters and setters
    public String getName(){return name;}
    public void setName(String newName){name = newName;}

    public String getMessage(){return message;}
    public void setMessage(String s){message = s;}

    public String getOwner(){return owner;}
    public void setOwner(String s){owner = s;}

    public String getTitle(){return title;}
    public void setTitle(String s){title = s;}

    public List<String> getParticipants(){return participants;}
    public void setParticipants(String s){
        if(!participants.contains(s)){ // Note : Possibly add a check for type or an overload
            participants.add(s);
        }
    }
    public void setParticipants(List<String> l){
        for(String x : l){
            if (!participants.contains(x)){
                participants.add(x);
            }
        }
    }

    public LocalDateTime getCreatedDate(){return createdDate;}
    public void setCreatedDate(){createdDate = LocalDateTime.now();}

    public LocalDateTime getScheduledDate(){return scheduledDate;}
    public void setScheduledDate(LocalDateTime d){scheduledDate = d;}
    //End of getter and setter
    

}
