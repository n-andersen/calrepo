package dataAccess;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import api_interface.IAppointment;
import dao.*;
public class Appointment_repo implements IAppointment {

    // Idea, return type - List<Appointment_dao>
    // Idea, hashmap might better convert to json
    // Also, for future reference, todo - split/ make new gets based on different parameters (eg. getByDate, getByOwner etc.)
    CalDB DBConn = new CalDB();
    public List<Appointment_dao> getAllAppointments(){
        try {
            List<Appointment_dao> resList = new ArrayList<>();
            String q = "Select * from appointments";
            Statement stmt = DBConn.conn.createStatement();
            ResultSet res = stmt.executeQuery(q);

            while (res.next()){
                resList.add(new Appointment_dao(res));
            }
            return resList;

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public List<Appointment_dao> getAppointmentsByOwner(Users_dao owner) {
        List<Appointment_dao> retList = new ArrayList<Appointment_dao>();
        try {
            String q = "Select * from appointments where owner = ?";
            //System.out.println(q); DEBUG
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setString(1, owner.getName());
            //System.out.println(stmt); DEBUG
            ResultSet res = stmt.executeQuery();

            while (res.next()) {
                retList.add(new Appointment_dao(res));
            }
            return retList;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }
    public List<Appointment_dao> getAppointmentsByDate(LocalDate target){
        List<Appointment_dao> retList = new ArrayList<>();
        try{
            String q = "Select * from appointments where date = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setObject(1,target);
            ResultSet res = stmt.executeQuery();

            while (res.next()){
                retList.add(new Appointment_dao(res));
            }
            return retList;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public Appointment_dao getAppointmentByID(Integer aid){
        try {
            String q = "Select * from appointments where aid = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setString(1,aid.toString());

            return new Appointment_dao(stmt.executeQuery());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public List<Users_dao> getParticipants(Appointment_dao target){
        try{
            List<Users_dao> retList = new ArrayList<>();
            String q = "Select uid from aParticipants where aid = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setInt(1,target.getId());
            ResultSet res = stmt.executeQuery();

            while (res.next()){
                retList.add(new Users_dao(res));
            }
            return retList;

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public Boolean createAppointment(Appointment_dao insert){
        try {
            String q = "Insert into appointments(title,owner,atext,date,startTime,endTime) " +
                    "values(?,?,?,?,?)";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setString(1,insert.getTitle());
            stmt.setString(2,insert.getOwner().getName());
            stmt.setString(3,insert.getText());
            stmt.setDate(4,java.sql.Date.valueOf(insert.getDate()));
            stmt.setTime(5,java.sql.Time.valueOf(insert.getStartTime()));
            stmt.setTime(6,java.sql.Time.valueOf(insert.getEndTime()));

            if(stmt.executeUpdate()>0){
                return true;
            }
            throw new RuntimeException("Invalid appointment object");

        }catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

}
