package api_interface;

import dao.Users_dao;

import java.util.List;

public interface IUsers {
    List<Users_dao> getAllUsers();
    Users_dao getUser(Integer uid);
    Users_dao getUserByName(String targetName);
    Boolean userExists(String targetName);
    Boolean createUser(Users_dao newUser);
}
