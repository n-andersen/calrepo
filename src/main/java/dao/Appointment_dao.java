package dao;


import java.security.PublicKey;
import java.sql.ResultSet;
import java.util.Date;
import java.time.*;

public class Appointment_dao {
    private Integer id;
    private String title,text;
    private LocalTime startTime,endTime;
    private LocalDate date;
    private Users_dao owner;

  public Appointment_dao(String ti,String te,LocalTime startTime,LocalTime endTime,LocalDate da, Users_dao owner)
    {
        setTitle(ti);
        setOwner(owner);
        setText(te);
        setStartTime(startTime);
        setEndTime(endTime);
        setDate(da);
    }
    public Appointment_dao(ResultSet res){
      try{
          setId(res.getInt("aid"));
          setTitle(res.getString("title"));
          setOwner(new Users_dao(res.getString("owner")));
          setText(res.getString("atext"));
          setStartTime(res.getTime("startTime").toLocalTime());
          setEndTime(res.getTime("endTime").toLocalTime());
          setDate(res.getDate("date").toLocalDate());
      }catch (Exception e){
          System.out.println(e.getMessage());
      }
    }
    public Appointment_dao(){
      setId(null);
      setTitle("Placeholder TEST");
      setOwner(new Users_dao("Ranni"));
      setText("This is a placeholder");
      setStartTime(LocalTime.of(0,0));
      setEndTime(LocalTime.of(1,0));
      setDate(LocalDate.of(1990,Month.JANUARY,1));
    }

    public Integer getId() {return id;}

    public String getText() {
        return text;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public Users_dao getOwner() {
        return owner;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() { return endTime; }

    public void setId(Integer id) {this.id = id;}

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setOwner(Users_dao owner) {
        this.owner = owner;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setStartTime(LocalTime time) {
        this.startTime = time;
    }

    public void setEndTime(LocalTime endTime) {this.endTime = endTime;}

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration(){
      if(startTime == null && endTime == null)
          return null;
      return ((endTime.getHour()-startTime.getHour())*60+(endTime.getMinute()-startTime.getMinute()));
    }
    @Override
    public String toString() {
        return "Appointment_dao{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", date=" + date +
                ", owner=" + owner +
                '}';
    }
}
