package dao;

public class AParticipants_dao {
    private Integer uid,aid;

    public Integer getAid() {
        return aid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "AParticipants_dao{" +
                "uid=" + uid +
                ", aid=" + aid +
                '}';
    }
}
