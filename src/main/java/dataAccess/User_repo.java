package dataAccess;

import api_interface.IUsers;
import dao.Users_dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class User_repo implements IUsers {
    CalDB DBConn = new CalDB();
    public Users_dao getUser(Integer uid){
        try{
            String q = "Select name from users where uid = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setInt(1,uid);
            ResultSet res = stmt.executeQuery(q);
            return new Users_dao(res);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public List<Users_dao> getAllUsers(){
        List<Users_dao> result = new ArrayList<>();
        try {
            String q = "Select name from users";
            Statement stmt = DBConn.conn.createStatement();
            ResultSet resSet = stmt.executeQuery(q);

            while (resSet.next()){
                result.add(new Users_dao(resSet));
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public Users_dao getUserByName(String target){
        try{
            String q ="Select name from users where name = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setString(1,target);
            return new Users_dao(stmt.executeQuery());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public Boolean createUser(Users_dao insert){
        try{
            String q = "Insert into users(name) values(?)";
            PreparedStatement stmt= DBConn.conn.prepareStatement(q);
            stmt.setString(1,insert.getName());

            if (stmt.executeUpdate() >0)
            return true;
        }catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
        return false;
    }
    public Boolean userExists(String target){
        try {
            String q = "Select name from users where name = ?";
            PreparedStatement stmt = DBConn.conn.prepareStatement(q);
            stmt.setString(1,target);
            ResultSet resultSet = stmt.executeQuery();
            if(resultSet.next())
                return true;
            return false;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
}
