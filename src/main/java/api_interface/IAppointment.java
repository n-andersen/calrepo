package api_interface;

import dao.Appointment_dao;
import dao.Users_dao;

import java.time.LocalDate;
import java.util.List;

public interface IAppointment {
    Appointment_dao getAppointmentByID(Integer aid);
    List<Appointment_dao> getAllAppointments();
    List<Appointment_dao> getAppointmentsByOwner(Users_dao owner);
    List<Appointment_dao> getAppointmentsByDate(LocalDate date);
    List<Users_dao> getParticipants(Appointment_dao targetAppointment);
    Boolean createAppointment(Appointment_dao newAppointment);
}
