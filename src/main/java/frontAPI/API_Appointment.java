package frontAPI;

import dao.Appointment_dao;
import dao.Users_dao;
import dataAccess.Appointment_repo;
import dataLogic.CalendarUtil;

import java.time.LocalDate;

//Make static?
public class API_Appointment {
    Appointment_repo repo = new Appointment_repo();
    public String getAppointments(Users_dao targetOwner){
        String result;
        try{
            result = repo.getAppointmentsByOwner(targetOwner).toString();
            return result;

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return null;
    }
    public String getParticipantCountDate(String targetDate){
        LocalDate date;
        try{
            //todo Insert function counter here - done?
            date = LocalDate.parse(targetDate);
            if(CalendarUtil.validateDate(targetDate)){
                return CalendarUtil.getCount(repo.getAppointmentsByDate(date)).toString();
            }else{
                throw new RuntimeException("Invalid date format - use year - month - day");
            }
        }catch (Exception e){
            return "0 :"+e.getMessage();
        }
    }
    public String createAppointment(String newAppointment){
        //todo Insert validation and interpreter here
        Appointment_dao newAppointmentObj = new Appointment_dao();
        try{
            return repo.createAppointment(newAppointmentObj).toString();
        }catch (Exception e){
            //Early error handling
            return "0 :" + e.getMessage();
        }
    }
}
